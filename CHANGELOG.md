## [Unreleased]

## [1.1.0] - 2025-03-04

- Add `latest_patch_for_version` method to retrieve the latest patch for a given minor version

## [1.0.1] - 2024-11-20

- Upgrade gitlab gem to 5.1
- Upgrade activesupport gem to 8.0

## [1.0.0] - 2024-07-04

- Upgrade Ruby to 3.3.1
- Upgrade gitlab gem to 4.20.1
- Add runbook for publishing new versions of this gem

## [0.2.9] - 2024-04-18

- Fixes `ReleaseCalculator` class to consider major milestones.

## [0.2.8]

- Yanked version

## [0.2.7] - 2024-03-06

- Add `next_patch_release_date` method to return the next best-effort date for patch releases

## [0.2.6] - 2024-02-22

- Add `available_versions` method to return the available GitLab versions (sorted by most recent)

## [0.2.5] - 2023-11-23

- Add methods to calculate the current version of a given date (`#current_minor_for_date`) and the previous minors for a specific version (`#previous_minors`)

## [0.2.4] - 2023-11-03

- Remove logic for static release date. All release dates will be calculated based on the third Thursday of the month.

## [0.2.3] - 2023-10-11

- Update ActiveSupport gem and loosen version restriction

## [0.2.2] - 2023-10-02

- Fixed `previous_version` to return the latest patch of the previous minor version.

## [0.2.1] - 2023-09-25

- `GitlabReleases.active_version` was updated to consider dates equal than the date provided. This mirrors the logic used in release-tools.

## [0.2.0] - 2023-08-31

- Gem renamed to `gitlab-releases` to align with GitLab gem naming conventions
- PRODUCTION_TOKEN renamed to RELEASES_GITLAB_READ_TOKEN

## [0.1.3] - 2023-08-22

- `version_for_date` and `previous_version` methods included.

## [0.1.2] - 2023-08-22

- Yanked version

## [0.1.1] - 2023-08-21

- Gem renamed to `releases` to account for new utility methods included (`active_version`,  `current_version`, `next_versions`)

## [0.1.0] - 2023-08-01

- Initial release. Starting with 16.6, GitLab monthly release date will be moved from the 22nd to the 3rd Thursday of each month, this gem automatically calculates the release dates and the associated version for 12 months in advance.
