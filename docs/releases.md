## Releases

To create a new version:

1. Adjust the [`VERSION` file] according to [Semantic Versioning].
2. Add the [Changelog] entry.
3. Update the [README] file with the new version.
4. Run `bundle install` in this gem's directory on your local environment, and check in the change to Gemfile.lock.
5. Create a merge request with the above changes and submit it to review. It is not required for the
    merge request to be merged in order to continue with the next steps.
6. On your local, build the new gem from the gemspec specification:

```
gem build gitlab-releases.gemspec
```

The above will generate a `gem` file like, `gitlab-releases-x.x.x.gem`, where `x.x.x` is the version
number set on the [`VERSION` file]. Now we can share the new gem version with the rest of the Ruby community. 
For the next steps you will need to have an account on [RubyGems.org].

7. Log into RubyGems

```
gem signin
```

8. Push the gem file to RubyGems

```
gem push gitlab-releases-x.x.x.gem
````

9. In a short time, the gem will be available for installation to anyone. Go to the the [gitlab-releases RubyGems.org site] to verify.
10. Update the [`release-tools` repository] with the new gem version or you can also wait for the renovate bot to open an MR.

[`VERSION` file]: https://gitlab.com/gitlab-org/ruby/gems/gitlab-releases/-/blob/main/lib/gitlab_releases/version.rb
[Semantic Versioning]: https://semver.org/
[Changelog]: https://gitlab.com/gitlab-org/ruby/gems/gitlab-releases/-/blob/main/CHANGELOG.md
[README]: https://gitlab.com/gitlab-org/ruby/gems/gitlab-releases/-/blob/main/README.md
[RubyGems.org]: https://rubygems.org/
[gitlab-releases RubyGems.org site]: https://rubygems.org/gems/gitlab-releases
[`release-tools` repository]: https://gitlab.com/gitlab-org/release-tools
