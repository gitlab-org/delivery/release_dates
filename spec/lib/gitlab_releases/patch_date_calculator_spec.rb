# frozen_string_literal: true

require 'spec_helper'
require 'rspec-parameterized'
require 'timecop'

RSpec.describe PatchDateCalculator do
  using RSpec::Parameterized::TableSyntax

  subject(:patch_date_calculator) { described_class.new }

  describe '#execute' do
    where(:upcoming_release_date, :current_date, :patch_date) do
      Date.parse('2024-04-18') | Date.parse('2024-03-27') | '2024-04-10'
      Date.parse('2024-04-18') | Date.parse('2024-04-10') | '2024-04-24'
      Date.parse('2024-05-16') | Date.parse('2024-04-24') | '2024-05-08'
      Date.parse('2024-05-16') | Date.parse('2024-05-08') | '2024-05-22'
      Date.parse('2024-06-20') | Date.parse('2024-05-22') | '2024-06-12'
      Date.parse('2024-06-20') | Date.parse('2024-06-12') | '2024-06-26'
      Date.parse('2024-07-18') | Date.parse('2024-06-26') | '2024-07-10'
      Date.parse('2024-07-18') | Date.parse('2024-07-10') | '2024-07-24'
      Date.parse('2024-08-15') | Date.parse('2024-07-24') | '2024-08-07'
      Date.parse('2024-08-15') | Date.parse('2024-08-07') | '2024-08-21'
    end

    with_them do
      before do
        allow(ReleaseVersions)
          .to receive(:upcoming_release_date)
          .and_return(upcoming_release_date)
      end

      it 'assigns the correct Wednesday' do
        Timecop.freeze(Time.local(current_date.year, current_date.month, current_date.day)) do
          expect(patch_date_calculator.execute).to eq(patch_date)
        end
      end
    end
  end
end
