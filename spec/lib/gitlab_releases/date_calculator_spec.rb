# frozen_string_literal: true

require 'spec_helper'

RSpec.describe DateCalculator do
  let(:next_version) { ReleaseVersion.new('16.6') }
  let(:candidate_date) { Date.parse('2023-11-01') }

  subject(:date_calculator) do
    described_class.new(
      next_version: next_version,
      candidate_date: candidate_date
    )
  end

  describe '#execute' do
    it 'returns third thursday of the month' do
      expect(date_calculator.execute).to eq('2023-11-16')
    end

    context 'when the version is greater than the updated release date' do
      let(:next_version) { ReleaseVersion.new('16.10') }
      let(:candidate_date) { Date.parse('2024-04-01') }

      it 'returns the 3rd thursday of the month' do
        expect(date_calculator.execute).to eq('2024-04-18')
      end
    end

    context 'when the first thursday is the first day of the month' do
      let(:next_version) { ReleaseVersion.new('16.9') }
      let(:candidate_date) { Date.parse('2024-02-01') }

      it 'returns the 3rd thursday  of the month' do
        expect(date_calculator.execute).to eq('2024-02-15')
      end
    end
  end
end
