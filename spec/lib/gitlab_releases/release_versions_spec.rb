# frozen_string_literal: true

require 'spec_helper'
require 'rspec-parameterized'
require 'timecop'

RSpec.describe ReleaseVersions do
  using RSpec::Parameterized::TableSyntax

  subject(:release_versions) { described_class }

  describe '.current_list' do
    it 'returns an array of all the versions', vcr: { cassette_name: 'versions/list' } do
      expect(release_versions.current_list).to be_a_instance_of(Array)
    end
  end

  describe '.next_versions' do
    it 'returns the next patch versions of the latest releases', vcr: { cassette_name: 'versions/list' } do
      expect(release_versions.next_versions)
        .to contain_exactly('16.2.9', '16.3.6', '16.4.2')
    end
  end

  describe '.current_version' do
    it 'returns the GitLab current version', vcr: { cassette_name: 'versions/list' } do
      expect(release_versions.current_version).to eq('16.4')
    end
  end

  describe '.next' do
    it 'returns an Array of the next patch versions' do
      versions = %w[1.0.0 1.1.0 1.1.1 1.2.3]

      expect(described_class.next(versions)).to eq(%w[1.0.1 1.1.1 1.1.2 1.2.4])
    end
  end

  describe '.latest' do
    it 'returns the latest versions grouped by minor version' do
      versions = %w[1.0.0 1.1.0 1.1.1 1.2.3]

      expect(described_class.latest(versions, 2)).to eq(%w[1.2.3 1.1.1])
    end
  end

  describe '.previous_release_date' do
    it 'returns the last release date', vcr: { cassette_name: 'versions/list' } do
      expect(release_versions.previous_release_date).to eq(Date.parse('2023-09-22'))
    end
  end

  describe '.upcoming_release_date' do
    it 'returns the upcoming release date', vcr: { cassette_name: 'releases/list' } do
      Timecop.freeze(Time.local(2024, 3, 4)) do
        expect(release_versions.upcoming_release_date).to eq(Date.parse('2024-03-21'))
      end
    end
  end

  describe '.active_version' do
    it 'returns the active_release', vcr: { cassette_name: 'releases/list' } do
      Timecop.freeze(Time.local(2023, 8, 21)) do
        expect(release_versions.active_version).to eq(ReleaseVersion.new('16.3'))
      end
    end
  end

  describe '.version_for_date' do
    where(:date, :version) do
      Date.parse('2023-08-17') | '16.3'
      Date.parse('2023-08-22') | '16.3'
      Date.parse('2023-08-23') | '16.4'
      Date.parse('2023-09-22') | '16.4'
      Date.parse('2023-09-23') | '16.5'
      Date.parse('2023-10-22') | '16.5'
      Date.parse('2023-10-23') | '16.6'
      Date.parse('2023-11-15') | '16.6'
      Date.parse('2023-11-16') | '16.6'
      Date.parse('2023-11-17') | '16.7'
    end

    with_them do
      it 'returns the release associated with the given date', vcr: { cassette_name: 'releases/list' } do
        expect(release_versions.version_for_date(date)).to eq(ReleaseVersion.new(version))
      end
    end
  end

  describe '.current_minor_for_date' do
    where(:date, :version) do
      Date.parse('2023-08-17') | '16.2'
      Date.parse('2023-08-22') | '16.3'
      Date.parse('2023-08-23') | '16.3'
      Date.parse('2023-09-22') | '16.4'
      Date.parse('2023-09-23') | '16.4'
      Date.parse('2023-10-22') | '16.5'
      Date.parse('2023-10-23') | '16.5'
      Date.parse('2023-11-15') | '16.5'
      Date.parse('2023-11-16') | '16.6'
      Date.parse('2023-11-17') | '16.6'
      Date.parse('2023-11-30') | '16.6'
      Date.parse('2023-12-13') | '16.6'
    end

    with_them do
      it 'returns the release associated with the given date', vcr: { cassette_name: 'releases/list' } do
        expect(release_versions.current_minor_for_date(date)).to eq(ReleaseVersion.new(version))
      end
    end
  end

  describe '.previous_version' do
    it 'returns the last patch of the previous minor version', vcr: { cassette_name: 'versions/list' } do
      expect(release_versions.previous_version).to eq(ReleaseVersion.new('16.3.5'))
    end
  end

  describe '.previous_minors' do
    where(:version, :result) do
      '16.6'  | ['16.6', '16.5', '16.4']
      '16.7'  | ['16.7', '16.6', '16.5']
      '16.10' | ['16.10', '16.9', '16.8']
      '17.1'  | ['17.1', '17.0', '16.11']
    end

    with_them do
      it 'returns the previous minor versions for the version', vcr: { cassette_name: 'releases/list' } do
        expect(release_versions.previous_minors(version)).to match_array(result)
      end
    end
  end

  describe '.available_versions' do
    it 'returns available versions sorted by most recent', vcr: { cassette_name: 'versions/list' } do
      versions = ['16.4.1', '16.4.0', '16.3.5', '16.3.4', '16.3.3']

      expect(release_versions.available_versions.take(5)).to eq(versions)
    end
  end

  describe '.latest_patch_for_version' do
    before do
      allow(release_versions).to receive(:available_versions).and_return(
        [
          '16.7.5', '16.7.4', '16.7.3', '16.7.2', '16.7.1', '16.7.0',
          '16.6.8', '16.6.7', '16.6.6', '16.6.5', '16.6.0',
          '16.5.3', '16.5.2', '16.5.1', '16.5.0',
          '16.4.1', '16.4.0',
          '16.3.0'
        ]
      )
    end

    it 'returns the latest patch for each minor version' do
      expect(release_versions.latest_patch_for_version('16.7')).to eq('16.7.5')
      expect(release_versions.latest_patch_for_version('16.6')).to eq('16.6.8')
      expect(release_versions.latest_patch_for_version('16.5')).to eq('16.5.3')
      expect(release_versions.latest_patch_for_version('16.4')).to eq('16.4.1')
      expect(release_versions.latest_patch_for_version('16.3')).to eq('16.3.0')
    end

    it 'returns nil for a non-existent version' do
      expect(release_versions.latest_patch_for_version('16.8')).to be_nil
    end
  end
end
