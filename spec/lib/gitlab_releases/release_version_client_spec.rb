# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseVersionClient do
  subject(:client) { described_class }

  describe '.versions' do
    it 'returns a list of versions', vcr: { cassette_name: 'versions/list' } do
      expect(client.versions).to be_a_instance_of(HTTParty::Response)
    end
  end
end
