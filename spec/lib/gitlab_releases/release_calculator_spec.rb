# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseCalculator do
  let(:current_version) { ReleaseVersion.new('16.11') }
  let(:release_date) { Date.parse('2024-04-18') }

  let(:releases) do
    {
      '16.11' => '2024-04-18',
      '17.0' => '2024-05-16',
      '17.1' => '2024-06-20',
      '17.2' => '2024-07-18',
      '17.3' => '2024-08-15',
      '17.4' => '2024-09-19',
      '17.5' => '2024-10-17',
      '17.6' => '2024-11-21',
      '17.7' => '2024-12-19',
      '17.8' => '2025-01-16',
      '17.9' => '2025-02-20',
      '17.10' => '2025-03-20',
      '17.11' => '2025-04-17'
    }
  end

  subject(:release_calculator) do
    described_class.new(
      current_version: current_version,
      release_date: release_date
    )
  end

  it 'returns a list of the next 12 releases' do
    expect(release_calculator.execute).to eq(releases)
  end
end
