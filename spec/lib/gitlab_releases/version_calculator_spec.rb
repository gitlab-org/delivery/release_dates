# frozen_string_literal: true

require 'spec_helper'

RSpec.describe VersionCalculator do
  let(:version) { ReleaseVersion.new('16.3') }
  let(:candidate_date) { Date.parse('2023-08-01') }

  subject(:version_calculator) do
    described_class.new(
      version: version,
      candidate_date: candidate_date
    )
  end

  it 'returns the next minor version' do
    expect(version_calculator.execute).to eq('16.4')
  end

  context 'when the candidate date falls one month before May' do
    let(:candidate_date) { Date.parse('2023-04-01') }

    it 'returns the next major version' do
      expect(version_calculator.execute).to eq('17.0')
    end
  end
end
