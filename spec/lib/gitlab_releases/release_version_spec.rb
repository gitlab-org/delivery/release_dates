# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseVersion do
  def version(version_string)
    described_class.new(version_string)
  end

  describe '#major' do
    it { expect(version('1.2.3').major).to eq(1) }
    it { expect(version('1.2.0-rc1').major).to eq(1) }
    it { expect(version('1.2.0-ee').major).to eq(1) }
    it { expect(version('wow.1').major).to eq(0) }
    it { expect(version('16.1.1').major).to eq(16) }
  end

  describe '#minor' do
    it { expect(version('1.2.3').minor).to eq(2) }
    it { expect(version('1.2.0-rc1').minor).to eq(2) }
    it { expect(version('1.2.0-ee').minor).to eq(2) }
    it { expect(version('wow.1').minor).to eq(0) }
    it { expect(version('16.1.1').minor).to eq(1) }
  end

  describe '#next_minor' do
    it 'returns next minor version' do
      expect(version('1.2').next_minor).to eq '1.3'
    end

    it 'returns next minor version when patch version is omitted' do
      expect(version('1.2').next_minor).to eq '1.3'
    end

    it 'returns next minor version when version is not a release' do
      expect(version('1.2.3-rc1').next_minor).to eq '1.3'
    end

    it 'returns next minor version when version is EE' do
      expect(version('1.2.3-ee').next_minor).to eq '1.3'
    end

    it 'returns next minor version when patch is > 0' do
      expect(version('1.2.3').next_minor).to eq '1.3'
    end
  end

  describe '#next_major' do
    it 'returns next minor version' do
      expect(version('1.2.0').next_major).to eq '2.0'
    end

    it 'returns next major version when version is not a release' do
      expect(version('1.2.3-rc1').next_major).to eq '2.0'
    end

    it 'returns next major version when version is EE' do
      expect(version('1.2.3-ee').next_major).to eq '2.0'
    end

    it 'returns next major version when patch is > 0' do
      expect(version('1.2.3').next_major).to eq '2.0'
    end
  end

  describe '#to_minor' do
    it 'returns the minor version' do
      expect(version('1.23.4').to_minor).to eq '1.23'
    end
  end

  describe '#previous_minor' do
    it 'returns the version of the previous minor release' do
      ver = version('12.4.0')

      expect(ver.previous_minor).to eq('12.3')
    end
  end

  describe '#previous_patch' do
    it 'returns nil when patch is missing' do
      expect(version('1.2').previous_patch).to be_nil
    end

    it 'returns nil when version is not a release' do
      expect(version('1.2.3-rc1').previous_patch).to eq '1.2.2'
    end

    it 'returns nil when version is EE' do
      expect(version('1.2.3-ee').previous_patch).to eq '1.2.2-ee'
    end

    it 'returns previous patch when patch is 0' do
      expect(version('1.2.0').previous_patch).to be_nil
    end

    it 'returns previous patch when patch is > 0' do
      expect(version('1.2.3').previous_patch).to eq '1.2.2'
    end
  end

  describe '#next_patch' do
    it 'returns nil when patch is missing' do
      expect(version('1.2').next_patch).to eq '1.2.1'
    end

    it 'returns nil when version is not a release' do
      expect(version('1.2.3-rc1').next_patch).to eq '1.2.4'
    end

    it 'returns nil when version is EE' do
      expect(version('1.2.3-ee').next_patch).to eq '1.2.4-ee'
    end

    it 'returns next patch when patch is 0' do
      expect(version('1.2.0').next_patch).to eq '1.2.1'
    end

    it 'returns next patch when patch is > 0' do
      expect(version('1.2.3').next_patch).to eq '1.2.4'
    end
  end

  describe '#ee?' do
    it 'returns true when EE' do
      expect(version('8.3.2-ee')).to be_ee
    end

    it 'returns false when not EE' do
      expect(version('8.3.2')).not_to be_ee
    end
  end

  describe '#patch?' do
    it 'is true for patch releases' do
      expect(version('1.2.3')).to be_patch
    end

    it 'is false for pre-releases' do
      expect(version('1.2.0-rc1')).not_to be_patch
    end

    it 'is false for minor releases' do
      expect(version('1.2.0')).not_to be_patch
    end

    it 'is false for invalid releases' do
      expect(version('wow.1')).not_to be_patch
    end
  end

  describe '#to_ee' do
    it 'returns self when already EE' do
      version = version('1.2.3-ee')

      expect(version.to_ee).to eql version
    end

    it 'returns an EE version when CE' do
      version = version('1.2.3')

      expect(version.to_ee.to_s).to eq '1.2.3-ee'
    end
  end
end
