# frozen_string_literal: true

require 'spec_helper'
require 'timecop'

RSpec.describe GitlabReleases do
  let(:release_versions) { stub_const('ReleaseVersions', spy) }
  let(:calculator) { stub_const('ReleaseCalculator', spy) }

  subject(:gitlab_releases) { described_class }

  describe '.upcoming_releases' do
    before do
      allow(release_versions)
        .to receive(:current_version)
        .and_return('16.4')

      allow(release_versions)
        .to receive(:previous_release_date)
        .and_return(Date.parse('2023-07-22'))
    end

    it 'calls release calculator' do
      expect(calculator).to receive(:execute)

      gitlab_releases.upcoming_releases
    end
  end

  describe '.current_version' do
    it 'returns the current GitLab version', vcr: { cassette_name: 'versions/list' } do
      expect(gitlab_releases.current_version).to eq('16.4')
    end
  end

  describe '.active_version' do
    it 'returns the active GitLab version', vcr: { cassette_name: 'releases/list' } do
      Timecop.freeze(Time.local(2023, 8, 21)) do
        expect(gitlab_releases.active_version).to eq(ReleaseVersion.new('16.3'))
      end
    end
  end

  describe '.version_for_date' do
    it 'returns the active version for the specified date', vcr: { cassette_name: 'releases/list' } do
      expect(gitlab_releases.version_for_date(Date.parse('2023-08-23'))).to eq('16.4')
      expect(gitlab_releases.version_for_date(Date.parse('2023-11-17'))).to eq('16.7')
    end
  end

  describe '.next_versions' do
    it 'returns the next patch versions of the latest releases', vcr: { cassette_name: 'versions/list' } do
      expect(gitlab_releases.next_versions)
        .to contain_exactly('16.2.9', '16.3.6', '16.4.2')
    end
  end

  describe '.previous_version' do
    it 'returns the last patch of the previous minor version', vcr: { cassette_name: 'versions/list' } do
      expect(gitlab_releases.previous_version).to eq('16.3.5')
    end
  end

  describe '.current_minor_for_date' do
    it 'returns the current version for the specified date', vcr: { cassette_name: 'releases/list' } do
      expect(gitlab_releases.current_minor_for_date(Date.parse('2023-08-30'))).to eq('16.3')
      expect(gitlab_releases.current_minor_for_date(Date.parse('2023-12-13'))).to eq('16.6')
      expect(gitlab_releases.current_minor_for_date(Date.parse('2023-12-22'))).to eq('16.7')
    end
  end

  describe '.previous_minors' do
    it 'returns the 3 previous minor versions for the specific version', vcr: { cassette_name: 'releases/list' } do
      expect(gitlab_releases.previous_minors('16.6')).to match_array(['16.6', '16.5', '16.4'])
    end
  end

  describe '.available_versions' do
    it 'returns the available versions up to a max of 50', vcr: { cassette_name: 'versions/list' } do
      expect(gitlab_releases.available_versions.size).to eq(50)
      expect(gitlab_releases.available_versions.take(10)).to eq(['16.4.1', '16.4.0', '16.3.5', '16.3.4', '16.3.3',
                                                                 '16.3.2', '16.3.1', '16.3.0', '16.2.8', '16.2.7'])
    end
  end

  describe '.next_patch_release_date' do
    it 'returns the next patch release date' do
      allow(release_versions)
        .to receive(:upcoming_release_date)
        .and_return(Date.parse('2024-05-16'))

      Timecop.freeze(Time.local(2024, 4, 24)) do
        expect(gitlab_releases.next_patch_release_date).to eq('2024-05-08')
      end
    end
  end
end
