# frozen_string_literal: true

RSpec.describe GitlabReleases do
  it "has a version number" do
    expect(GitlabReleases::VERSION).not_to be nil
  end
end
