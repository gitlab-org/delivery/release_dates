[![Gem Version](https://badge.fury.io/rb/gitlab-releases.svg)](https://badge.fury.io/rb/gitlab-releases)

# Releases

Library to interact with GitLab releases and versions. The information is fetched from two sources:

1. [versions.gitlab.com](https://version.gitlab.com/)
1. [`releases.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/releases.yml)

## Installation

Gemfile:

```
gem 'gitlab-releases', '~> 1.1.0'
```

Install:
```
$ gem install gitlab-releases
```

or alternatively to automatically initialize the gem in your bundler project:

```ruby
# add to your Gemfile
gem 'gitlab-releases', require: 'gitlab_releases'
```

## Usage

There are different methods available:

* `upcoming_releases` - Returns the GitLab release date and the associated version for the next 12 months.
* `active_version`  - Returns the active GitLab version.
* `current_version` - Returns the current GitLab minor version.
* `next_versions` - Returns the next GitLab patch versions (for patch and security releases).
* `previous_version` - Returns the latest patch of the previous minor version.
* `version_for_date` - Returns the active GitLab version for the given date
* `current_minor_for_date` - Returns the current GitLab minor for the given date
* `previous_minors` - Returns the previous minors for the given version
* `next_patch_release_date` - Calculates the next best effort date for a GitLab patch release.
* `latest_patch_for_version` - Returns the latest patch for the given minor version

To make use of it:

1. Create a personal access token with `read_api` scope.
1. Store it in an environment variable called `RELEASES_GITLAB_READ_TOKEN`.
1. Open up an irb session:

```ruby
> require 'gitlab_releases'
> GitlabReleases.upcoming_releases
=>
{
 "16.5"=>"2023-10-22",
 "16.6"=>"2023-11-16",
 "16.7"=>"2023-12-21",
 "16.8"=>"2024-01-18",
 "16.9"=>"2024-02-15",
 "16.10"=>"2024-03-21",
 "16.11"=>"2024-04-18",
 "17.0"=>"2024-05-16",
 "17.1"=>"2024-06-20",
 "17.2"=>"2024-07-18",
 "17.3"=>"2024-08-15",
 "17.4"=>"2024-09-19"
}
> GitlabReleases.active_version
 => "16.6"
> GitlabReleases.current_version
=> "16.5"
> GitlabReleases.next_versions
=> ["16.5.2", "16.4.3", "16.3.7"]
> GitlabReleases.previous_version
=> "16.4.2"
> GitlabReleases.available_versions
=> ["16.4.1", "16.4.0", "16.3.5", "16.3.4", ...] # Up to 50 versions
> GitlabReleases.version_for_date('2023-11-23')
=> "16.7"
> GitlabReleases.current_minor_for_date('2023-11-23')
=> "16.6"
> GitlabReleases.previous_minors('16.6')
=> ["16.6", "16.5", "16.4"]
> GitlabReleases.next_patch_release_date
=> "2024-03-13"
> GitlabReleases.latest_patch_for_version('16.6')
=> "16.6.10"

```

## Contributing

Bug reports and feature requests are welcome on the [Delivery team tracker]. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/gitlab-org/ruby/gems/gitlab-releases/-/blob/main/CODE_OF_CONDUCT.md).

## Releases

See the [Releases documentation].

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Releases project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/gitlab-org/ruby/gems/gitlab-releases/-/blob/main/CODE_OF_CONDUCT.md).

[Delivery team tracker]: https://gitlab.com/gitlab-com/gl-infra/delivery
[Releases documentation]: https://gitlab.com/gitlab-org/ruby/gems/gitlab-releases/-/blob/main/docs/releases.md
