# frozen_string_literal: true

require 'version_sorter'
require 'retriable'
require 'http'
require_relative 'release_version_client'

# Wrapper to interact with GitLab release versions. Versions are fetched
# from two sources:
#
# 1. versions.gitlab.com => Used to interact with previous GitLab versions
# 1. releases.yml => Used to interact with upcoming GitLab releases
class ReleaseVersions
  # The base interval for retrying operations that failed, in seconds.
  RETRY_INTERVAL = 5

  # Releases.yml file.
  RELEASES_YAML = 'https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/releases.yml'

  # Returns the list of versions from versions.gitlab.com
  def self.current_list
    raw_versions.collect(&:version)
  end

  # Get the next three patch versions
  def self.next_versions
    self.next(latest(current_list, 3))
  end

  # Given an Array of version numbers, return the next patch versions
  #
  # Example:
  #
  #   next(['1.0.0', '1.1.0', '1.1.1', '1.2.3'])
  #   => ['1.0.1', '1.1.1', '1.1.2', '1.2.4']
  def self.next(versions)
    versions.map do |version|
      ReleaseVersion.new(version).next_patch
    end
  end

  # Given an Array of version strings, find the `count` latest by minor number
  #
  # versions - Array of version strings
  # count    - Number of versions to return (default: 3)
  #
  # Example:
  #
  #   latest(['1.0.0', '1.1.0', '1.1.1', '1.2.3'], 3)
  #   => ['1.2.3', '1.1.1', '1.0.0']
  def self.latest(versions, count = 3)
    ::VersionSorter.rsort(versions).uniq do |version|
      version.split('.').take(2)
    end.take(count)
  end

  # Returns the available GitLab versions as an Array
  #
  # Example:
  #
  #   available_versions
  #   => ['1.2.3', '1.1.1', '1.0.0']
  def self.available_versions
    ::VersionSorter.rsort(current_list)
  end

  # Returns the current GitLab minor version
  def self.current_version
    ReleaseVersion.new(next_versions.first.to_minor)
  end

  # Returns the release date of the current version
  def self.previous_release_date
    date = raw_versions
      .find { |version| version.version == "#{current_version}.0" }
      .created_at

    Date.parse(date)
  end

  # Returns the release date of the upcoming (active) version
  def self.upcoming_release_date
    releases_yaml
      .find { |release| release['version'] == active_version }
      .fetch('date')
  end

  # Returns the active minor GitLab Version
  def self.active_version
    version_for_date(DateTime.now)
  end

  # Returns the scheduled major.minor for the given date
  def self.version_for_date(date)
    # Returns the active release associated with the date by calling `releases_yaml` and fetching the
    # release whose date is greater or equal than the given date.
    #
    # For example:
    #
    # | Version | Date           |
    # |:--------|:---------------|
    # | 16.3    | August 22nd    |
    # | 16.4    | September 22nd |
    # | 16.5    | October 22nd   |
    # | 16.6    | November 16th  |
    # | 16.7    | December 21st  |
    #
    # * For August 21st, the release should be 16.3
    # * For August 22nd, the release should be 16.3
    # * For August 23rd, the release should be 16.4
    # * For November 17th, the release should be 16.7
    return nil if releases_yaml.empty?

    row = releases_yaml.find { |release| release['date'] >= date.to_date }

    ReleaseVersion.new(row['version'])
  end

  # Returns the current major.minor for the given date
  def self.current_minor_for_date(date)
    # Returns the current version associated with the date by calling `releases_yaml`
    # and fetching the version whose release date is less or equal to the given date.
    #
    # For example:
    #
    # | Version | Date           |
    # |:--------|:---------------|
    # | 16.3    | August 22nd    |
    # | 16.4    | September 22nd |
    # | 16.5    | October 22nd   |
    # | 16.6    | November 16th  |
    # | 16.7    | December 21st  |
    #
    # * For August 21st, the release should be 16.2
    # * For August 22nd, the release should be 16.3
    # * For August 23rd, the release should be 16.3
    # * For November 17th, the release should be 16.6
    return nil if releases_yaml.empty?

    row = releases_yaml
      .reverse
      .detect { |release| date.to_date >= release['date'] }

    ReleaseVersion.new(row['version'])
  end

  # Returns the last patch of the previous minor version
  #
  # For example, if the current version is 16.4, then
  # the previous version would be 16.3 and the last registered patch is 16.3.5
  #
  # => previous_version
  # => '16.3.5'
  def self.previous_version
    next_versions.second.previous_patch
  end

  # Returns the latest patch for a given minor version
  #
  # For example:
  # - If the given version is 16.7 and available versions include 16.7.0, 16.7.1, 16.7.2
  #   it will return 16.7.2
  # - If the given version is 16.5 and available versions include 16.5.0, 16.5.1
  #   it will return 16.5.1
  def self.latest_patch_for_version(version)
    minor_version = ReleaseVersion.new(version).to_minor

    available_versions.find { |v| v.start_with?("#{minor_version}.") }
  end

  def self.sort(versions)
    ::VersionSorter.sort(versions).uniq
  end

  # Returns the previous major.minor versions of the major.minor version based on the
  # releases.yml data
  #
  # For example:
  # - If the current version is 16.7, the previous patch versions are 16.7, 16.6, and 16.5
  # - If the current version is 17.1, the previous patch versions are 17.1, 16.11, and 16.10
  def self.previous_minors(version)
    versions = releases_yaml.map { |release| release['version'] }
      .reverse

    versions[versions.index(version), 3]
  end

  def self.raw_versions
    ReleaseVersionClient.versions
  end

  def self.releases_yaml
    @releases_yaml ||=
      begin
        data = YAML.safe_load(releases_information) || []
        data.map { |release| release['date'] = Date.parse(release['date']) }
        data.sort_by { |release| release['date'] }
      rescue StandardError
        []
      end
  end

  def self.releases_information
    Retriable.retriable(base_interval: RETRY_INTERVAL) do
      HTTP.get(RELEASES_YAML).to_s
    end
  end

  private_class_method :raw_versions, :releases_yaml, :releases_information
end
