# frozen_string_literal: true

require 'gitlab/request'
require 'gitlab'

class ReleaseVersionClient < Gitlab::Request
  base_uri 'https://version.gitlab.com'
  headers 'Private-Token' => -> { api_token }

  # Get the latest version information
  #
  # Returns an Array of Gitlab::ObjectifiedHash objects
  def self.versions
    get('/api/v1/versions', query: { per_page: 50 })
  end

  def self.api_token
    ENV.fetch('RELEASES_GITLAB_READ_TOKEN') do |name|
      raise "Must specify `#{name}` environment variable!"
    end
  end

  private_class_method :api_token
end
