# frozen_string_literal: true

module GitlabReleases
  VERSION = "1.1.0"
end
