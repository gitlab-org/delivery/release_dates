# frozen_string_literal: true

class ReleaseVersion < String
  VERSION_REGEX = /
  \A(?<major>\d+)
  \.(?<minor>\d+)
  (\.(?<patch>\d+))?
    (-(?<rc>rc(?<rc_number>\d*)))?
    (-\h+\.\h+)?
    (-ee|\.ee\.\d+)?\z
  /x

  def major
    @major ||= extract_from_version(:major).to_i
  end

  def minor
    @minor ||= extract_from_version(:minor).to_i
  end

  def patch
    @patch ||= extract_from_version(:patch).to_i
  end

  def next_minor
    "#{major}.#{minor + 1}"
  end

  def next_major
    "#{major + 1}.0"
  end

  def to_minor
    "#{major}.#{minor}"
  end

  def previous_minor
    "#{major}.#{minor - 1}"
  end

  def previous_patch
    return unless patch?

    new_patch = self.class.new("#{major}.#{minor}.#{patch - 1}")

    ee? ? new_patch.to_ee : new_patch
  end

  def next_patch
    new_patch = self.class.new("#{major}.#{minor}.#{patch + 1}")

    ee? ? new_patch.to_ee : new_patch
  end

  def ee?
    end_with?('-ee')
  end

  def patch?
    patch.positive?
  end

  def to_ee
    return self if ee?

    self.class.new("#{self}-ee")
  end

  private

  # rubocop:disable Lint/SafeNavigation
  def extract_from_version(part, fallback: 0)
    match_data = self.class::VERSION_REGEX.match(self)

    if match_data && match_data.names.include?(part.to_s) && match_data[part]
      String.new(match_data[part])
    else
      fallback
    end
  end
  # rubocop:enable Lint/SafeNavigation
end
