# frozen_string_literal: true

require_relative 'date_calculator'
require_relative 'version_calculator'

class ReleaseCalculator
  def initialize(current_version:, release_date:)
    @current_version = current_version
    @release_date = release_date
    @release_dates = {}
  end

  def execute
    @release_dates[current_version] = release_date.strftime('%Y-%m-%d')
    next_version = calculate_release_version(current_version, release_date)

    period(release_date, release_date + 1.year).each do |candidate_date|
      @release_dates[next_version] = calculate_release_date(next_version, candidate_date)
      next_version = calculate_release_version(next_version, candidate_date)
    end

    @release_dates
  end

  private

  attr_reader :current_version, :release_date, :release_dates

  def period(start_date, end_date)
    (start_date..end_date)
      .select { |date| date.day == 1 }
      .flatten
  end

  def calculate_release_date(next_version, candidate_date)
    DateCalculator.new(
      next_version: next_version,
      candidate_date: candidate_date
    ).execute
  end

  def calculate_release_version(next_version, candidate_date)
    VersionCalculator.new(
      version: next_version,
      candidate_date: candidate_date
    ).execute
  end
end
