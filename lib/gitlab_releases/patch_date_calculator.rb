# frozen_string_literal: true

require 'active_support/all'

# Calculates the next patch release date. Patch releases are scheduled
# twice a month based on the monthly release:
#
# - On the second Wednesday of the month (the week before the monthly release)
# - On the fourth Wednesday of the month (the week after the monthly release)
class PatchDateCalculator
  def initialize
    @next_release_date = ReleaseVersions.upcoming_release_date
  end

  def execute
    next_patch_date.strftime('%Y-%m-%d')
  end

  private

  attr_reader :next_release_date

  def next_patch_date
    if current_date >= prev_wednesday
      next_wednesday
    else
      prev_wednesday
    end
  end

  def current_date
    Date.today
  end

  def prev_wednesday
    next_release_date.prev_week(:wednesday)
  end

  def next_wednesday
    next_release_date.next_week(:wednesday)
  end
end
