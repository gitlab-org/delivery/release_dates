# frozen_string_literal: true

class VersionCalculator
  MAJOR_RELEASE_MONTH = 5 # represents the month major releases are performed, in this case May

  def initialize(version:, candidate_date:)
    @version = version
    @candidate_date = candidate_date
  end

  def execute
    if candidate_date.month == (MAJOR_RELEASE_MONTH - 1)
      next_major
    else
      next_minor
    end
  end

  private

  attr_reader :version, :candidate_date

  def next_major
    ReleaseVersion.new(version).next_major
  end

  def next_minor
    ReleaseVersion.new(version).next_minor
  end
end
