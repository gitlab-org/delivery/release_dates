# frozen_string_literal: true

require 'active_support/all'

class DateCalculator
  # @param version [ReleaseVersion]
  # @param candidate_date [Date] - The month to calculate the release date. It should be
  # the first day of the month.
  def initialize(next_version:, candidate_date:)
    @next_version = next_version
    @candidate_date = candidate_date
  end

  def execute
    calculate_third_thursday_of_month.strftime('%Y-%m-%d')
  end

  private

  attr_reader :next_version, :candidate_date

  # If the first day of the month is a Thursday, it fetches the subsequent Two thursdays,
  # if not searches for the 3rd Thursday of the month.
  def calculate_third_thursday_of_month
    iterations = candidate_date.thursday? ? 2 : 3
    third_thursday = candidate_date

    iterations.times do
      third_thursday = third_thursday.next_occurring(:thursday)
    end

    third_thursday
  end
end
