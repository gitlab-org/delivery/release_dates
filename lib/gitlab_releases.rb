# frozen_string_literal: true

require_relative 'gitlab_releases/version'
require_relative 'gitlab_releases/release_version'
require_relative 'gitlab_releases/release_versions'
require_relative 'gitlab_releases/release_calculator'
require_relative 'gitlab_releases/patch_date_calculator'

module GitlabReleases
  def self.upcoming_releases
    ReleaseCalculator.new(
      current_version: current_version,
      release_date: release_date
    ).execute
  end

  def self.current_version
    ReleaseVersions.current_version
  end

  def self.active_version
    ReleaseVersions.active_version
  end

  def self.version_for_date(date)
    ReleaseVersions.version_for_date(date)
  end

  def self.next_versions
    ReleaseVersions.next_versions
  end

  def self.previous_version
    ReleaseVersions.previous_version
  end

  def self.current_minor_for_date(date)
    ReleaseVersions.current_minor_for_date(date)
  end

  def self.previous_minors(version)
    ReleaseVersions.previous_minors(version)
  end

  def self.available_versions
    ReleaseVersions.available_versions
  end

  def self.next_patch_release_date
    PatchDateCalculator.new.execute
  end

  def self.release_date
    ReleaseVersions.previous_release_date
  end

  def self.latest_patch_for_version(version)
    ReleaseVersions.latest_patch_for_version(version)
  end

  private_class_method :release_date
end
